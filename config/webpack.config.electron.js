const { merge } = require('webpack-merge');
const Dotenv = require('dotenv-webpack');
const WebpackBar = require('webpackbar');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const nodeExternals = require('webpack-node-externals');
const baseConfig = require('./webpack.config.base');

const path = require('path');
const env = process.env.NODE_ENV;
module.exports = merge(baseConfig, {
  entry: './electron/main.ts',
  output: {
    path: path.resolve(__dirname, '../build_electron'),
    clean: false,
    filename: 'main.js'
  },
  target: 'electron-main',
  mode: env === 'development' ? 'none' : 'production',
  performance: {
    hints: false
  },
  cache: {
    // 磁盘存储
    type: 'filesystem',
    buildDependencies: {
      // 当配置修改时，缓存失效
      config: [__filename]
    }
  },
  devtool: false,
  externals: [nodeExternals()],
  node: {
    __dirname: false,
    __filename: false
  },
  plugins: [
    new Dotenv({
      path: path.resolve(__dirname, '../.env.production')
    }),
    new CleanWebpackPlugin({
      dry: false,
      cleanOnceBeforeBuildPatterns: ['../build_electron'],
      cleanAfterEveryBuildPatterns: ['../build_electron/index.html'],
      dangerouslyAllowCleanPatternsOutsideProject: true
    }),
    new WebpackBar({ name: '主进程', color: 'red' })
  ]
});
