/* eslint-disable @typescript-eslint/no-var-requires */
const { ipcRenderer } = require('electron');
const { getCurrentWindow } = require('@electron/remote');
window.api = {
  operateWindow: (type) => {
    ipcRenderer.send('operateWindow', type);
  },
  isMaximze: () => {
    return getCurrentWindow().isMaximized();
  }
};
export {};
